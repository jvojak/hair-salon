<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'price', 'description', 'last_name', 'duration', 'available'
    ];

    /**
     * Get appointments for that service.
     */
    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }
}
