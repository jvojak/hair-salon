<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DateTime;

class Barber extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'avatar', 'active'
    ];

    /**
     * Get barber appointments.
     */
    public function appointments()
    {
        return $this->hasMany('App\Appointment');
    }

    /**
     * Get barber worktimes.
     */
    public function worktimes()
    {
        return $this->hasMany('App\Worktime');
    }

    /*
     * Check if barber has active appointments
     */
    public function hasAppointments()
    {
        foreach($this->appointments as $appointment)
        {
            if (new DateTime() < new DateTime($appointment->time)) {
                return true;
            }
        }
        return false;
    }

    public function getFullNameAttribute($value)
    {
        return $this->first_name." ".$this->last_name;
    }
}
