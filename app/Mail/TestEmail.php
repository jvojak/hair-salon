<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TestEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = 'josip@vojak.com';
        $subject = 'Potvrda rezervacije usluge!';
        $name = 'Hair Salon';
        
        return $this->view('email.layout')
                    ->from($address, $name)
                    ->cc($address, $name)
                    ->bcc($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([ 'bodyMessage' => $this->data['message'],
                    'barber' => $this->data['barber'],
                    'service' => $this->data['service'],
                    'price' => $this->data['price'],
                    'time' => $this->data['time'],
                    ]);
    }
}
