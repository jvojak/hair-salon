<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Worktime extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'barber_id', 'date', 'start_hour', 'end_hour', 'is_weekend'
    ];

    /**
     * Get barber for appointment.
     */
    public function barber()
    {
        return $this->belongsTo('App\Barber');
    }
}
