<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'service_id', 'barber_id', 'first_name', 'last_name', 'email', 'contact', 'time', 'status'
    ];

    /**
     * Get service for appointment.
     */
    public function service()
    {
        return $this->belongsTo('App\Service');
    }

    /**
     * Get barber for appointment.
     */
    public function barber()
    {
        return $this->belongsTo('App\Barber');
    }
}
