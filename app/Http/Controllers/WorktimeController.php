<?php

namespace App\Http\Controllers;

use App\Worktime;
use Illuminate\Http\Request;

class WorktimeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'barber_id' => 'required',
            'date' => 'required|date',
            'start_hour' => 'required',
            'end_hour' => 'required',
        ]);

        $worktime = Worktime::create([
            'barber_id' => $request->barber_id,
            'date' => $request->date,
            'start_hour' => $request->start_hour,
            'end_hour' => $request->end_hour,
            'is_weekend' => 0
        ]);

        return response()->json([
            'message' => 'Successfully created new work time!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Worktime  $worktime
     * @return \Illuminate\Http\Response
     */
    public function show(Worktime $worktime)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Worktime  $worktime
     * @return \Illuminate\Http\Response
     */
    public function edit(Worktime $worktime)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Worktime  $worktime
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Worktime $worktime)
    {
        $request->validate([
            'barber_id' => 'required',
            'date' => 'required|date',
            'start_hour' => 'required',
            'end_hour' => 'required',
            ]);

        $worktime->fill([
            'barber_id' => $request->barber_id,
            'date' => $request->date,
            'start_hour' => $request->start_hour,
            'end_hour' => $request->end_hour,
        ]);

        $worktime->save();

        return response()->json([
            'message' => 'Successfully updated worktime!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Worktime  $worktime
     * @return \Illuminate\Http\Response
     */
    public function destroy(Worktime $worktime)
    {
        if($worktime->barber->hasAppointments())
            return response()->json([
                'message' => 'Barber has active appointments! Please remove them first!'
            ]);

        $worktime->delete();
        return response()->json([
            'message' => 'Successfully removed worktime!'
        ]);
    }
}
