<?php

namespace App\Http\Controllers;

use App\Barber;
use Image;
use Storage;
use Illuminate\Http\Request;

class BarberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Barber::latest('updated_at')->get();
        return view('admin.barbers.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.barbers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
        ]);
        
        $avatar = null;
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar')->hashName();
            $image = Image::make($request->file('avatar'));
            Storage::put('/public/avatars/'.$avatar, (string) $image->resize(350, 350)->encode('jpg', 90));
            Storage::put('/public/avatars/128x128/'.$avatar, (string) $image->resize(128, 128)->encode('jpg', 80));
        }

        $barber = Barber::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'avatar' => $avatar,
            'active' => 1 // barber is active
        ]);

        return redirect()->route('admin.barbers.index')->with('status', 'Successfully stored new barber!');

        return response()->json([
            'message' => 'Successfully stored new barber!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barber  $barber
     * @return \Illuminate\Http\Response
     */
    public function show(Barber $barber)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barber  $barber
     * @return \Illuminate\Http\Response
     */
    public function edit(Barber $barber)
    {
        return view('admin.barbers.edit', compact('barber'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barber  $barber
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Barber $barber)
    {
        $request->validate([
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:2',
            'active' => 'required'
        ]);
        
        $avatar = null;

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar')->hashName();
            $image = Image::make($request->file('avatar'));
            Storage::put('/public/avatars/'.$avatar, (string) $image->resize(350, 350)->encode('jpg', 90));
            Storage::put('/public/avatars/128x128/'.$avatar, (string) $image->resize(128, 128)->encode('jpg', 80));
        }

        $barber->fill([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'avatar' => $avatar,
            'active' => $request->active // barber is active
        ]);
        $barber->save();

        return redirect()->route('admin.barbers.index')->with('status', 'Successfully updated barber!');

        return response()->json([
            'message' => 'Successfully updated barber info!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barber  $barber
     * @return \Illuminate\Http\Response
     */
    public function destroy(Barber $barber)
    {
        if($barber->hasAppointments()) {

            return redirect()->route('admin.barbers.index')->with('status', 'Barber has active appointments! Please remove them first!');
            return response()->json([
                'message' => 'Barber has active appointments! Please remove them first!'
            ]);
        }
        $barber->delete();

        return redirect()->route('admin.barbers.index')->with('status', 'Successfully removed barber!');
        return response()->json([
            'message' => 'Successfully removed barber!'
        ]);
    }
}
