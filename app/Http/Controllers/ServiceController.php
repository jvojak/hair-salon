<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Service::latest('updated_at')->get();
        return view('admin.services.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2',
            'price' => 'required',
            'duration' => 'required'
        ]);

        $service = Service::create([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'available' => 1,
            'duration' => $request->duration
        ]);

        return redirect()->route('admin.services.index')->with('status', 'Successfully created a new service!');

        return response()->json([
            'message' => 'Successfully created a new service!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service)
    {
        $request->validate([
            'name' => 'required|min:2',
            'price' => 'required',
            'available' => 'required|boolean',
            'duration' => 'required'
        ]);

        $service->fill([
            'name' => $request->name,
            'price' => $request->price,
            'description' => $request->description,
            'available' => $request->available,
            'duration' => $request->duration
        ]);

        $service->save();

        return redirect()->route('admin.services.index')->with('status', 'Successfully updated a service!');

        return response()->json([
            'message' => 'Successfully updated a service!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //return redirect()->route('admin.services.index')->with('status', 'Successfully created a new service!');
    }
}
