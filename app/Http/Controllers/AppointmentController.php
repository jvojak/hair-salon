<?php

namespace App\Http\Controllers;

use App\Appointment;
use App\Notification;
use Illuminate\Http\Request;
use App\Mail\TestEmail;
use App\Events\NewServiceOrdered;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $appointments = Appointment::latest('time')->orderBy('time', 'asc')->get();
        return view('admin.appointments.index', compact('appointments'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function showAll()
    {
        $appointments = Appointment::latest('time')->get();
        return view('admin.appointments.all', compact('appointments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'service_id' => 'required',
            'barber_id' => 'required',
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:3',
            'email' => 'required',
            'contact' => 'required',
            'time' => 'required|date'
        ]);

        $appointment = Appointment::create([
            'service_id' => $request->service_id,
            'barber_id' => $request->barber_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'contact' => $request->contact,
            'time' => $request->time,
        ]);

        Notification::create([
            'title' => $request->first_name.' '.$request->last_name,
            'body' => 'je rezervirao/la termin'.' '.$request->time
        ]);
        
        $data = [
            'message' => 'Vaša rezervacija je uspješna!',
            'barber' => $appointment->barber->full_name,
            'service' => $appointment->service->name,
            'price' => $appointment->service->price,
            'time' => $appointment->service->time,
        ];
        \Mail::to('josipvojak@gmail.com')->send(new TestEmail($data));

        NewServiceOrdered::dispatch($appointment);

        return redirect()->route('admin.dash')->with('status', 'Successfully created an appointment!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit(Appointment $appointment)
    {
        $barbers = \App\Barber::all();
        $services = \App\Service::all();
        return view('admin.appointments.edit', compact('appointment', 'barbers', 'services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Appointment $appointment)
    {
        $request->validate([
            'service_id' => 'required',
            'barber_id' => 'required',
            'first_name' => 'required|min:2',
            'last_name' => 'required|min:3',
            'email' => 'required',
            'contact' => 'required',
            'time' => 'required|date', 
            'status' => 'required'
            ]);

        $appointment->fill([
            'service_id' => $request->service_id,
            'barber_id' => $request->barber_id,
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'contact' => $request->contact,
            'time' => $request->time,
            'status' => $request->status
        ]);

        $appointment->save();

        return redirect()->route('admin.appointments.index')->with('status', 'Successfully updated barber!');

        return response()->json([
            'message' => 'Successfully updated an appointment!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Appointment $appointment)
    {
        dd('usao');
    }
}
