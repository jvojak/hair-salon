<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyWorktimesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('worktimes', function (Blueprint $table) {
            $table->dropColumn(['date', 'start_hour', 'end_hour', 'is_weekend']);
            $table->string('year');
            $table->string('week_of_year');
            $table->string('shift');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('worktimes', function (Blueprint $table) {
            //
        });
    }
}
