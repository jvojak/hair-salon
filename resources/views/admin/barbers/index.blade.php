@extends('admin.default')

@section('page-header')
    Users <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

    <div class="mB-20">
        <a href="{{ route(ADMIN . '.barbers.create') }}" class="btn btn-info">
            {{ trans('app.add_button') }}
        </a>
    </div>


    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th style="width: 200px;">Ime</th>
                    <th style="width: 200px;">Prezime</th>
                    <th style="width: 150px;">Aktivan</th>
                    <th style="width: 180px;">Akcije</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>Ime</th>
                    <th>Prezime</th>
                    <th>Aktivan</th>
                    <th>Akcije</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td><a href="{{ route(ADMIN . '.barbers.edit', $item->id) }}">{{ $item->first_name }}</a></td>
                        <td>{{ $item->last_name }}</td>
                        <td>
                        @if($item->active)
                            <span class="peer">
                                <span class="badge badge-pill badge-success lh-0 p-10">Aktivan</span>
                            </span>
                        @else
                            <span class="peer">
                                <span class="badge badge-pill badge-danger lh-0 p-10">Neaktivan</span>
                            </span>
                        @endif
                        </td>
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.barbers.edit', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.barbers.destroy', $item->id), 
                                        'method' => 'DELETE',
                                        ]) 
                                    !!}

                                        <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>
                                        
                                    {!! Form::close() !!}
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>

@endsection