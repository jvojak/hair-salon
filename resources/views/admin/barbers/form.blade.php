<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
		<a href="" class="peers fxw-nw td-n p-20 bdB c-grey-800 cH-blue bgcH-grey-100 d-flex justify-content-center" >
			<div class="peer mR-15">
				<img class="bdrs-50p" src="@if(empty($barber)) https://randomuser.me/api/portraits/men/1.jpg @else {{ asset('/storage/public/avatars/128x128/'.$barber->avatar) }} @endif" alt="">
				<div class="p-image" style="position: absolute;
  color: #666666;
  transition: all .3s cubic-bezier(.175, .885, .32, 1.275);">
				<i class="fa fa-camera upload-button"></i>
				<input name="avatar" class="file-upload" type="file" accept="image/*"/>
			</div>
			</div>
			
		</a>
			{!! Form::myInput('text', 'first_name', 'Ime') !!}
			{!! Form::myInput('text', 'last_name', 'Prezime') !!}
			

			<div class="form-group row">
			<div class="col-sm-2">Barber aktivan?</div>
			<div class="col-sm-10">
				<div class="form-check">
				<label class="form-check-label">
					{{Form::hidden('active',0)}}
					{!! Form::checkbox('active') !!} 
				</label>
				</div>
			</div>
			</div>
		</div>  
	</div>
</div>