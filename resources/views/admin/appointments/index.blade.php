@extends('admin.default')

@section('css')
<style>
.fc-title {
    color: #fff;
}

.fc-time,
.fc-time span {
    color: #fff;
}
</style>
@endsection

@section('content')

<script>
    window.addEventListener('load', function load() {
    const loader = document.getElementById('loader');
    setTimeout(function() {
        loader.classList.add('fadeOut');
    }, 300);
    });
</script>

<main class='main-content bgc-grey-100' style="padding:0px;">
    <div id='mainContent'>
    <div class="container-fluid">
        <div class="row">
        <div class="col-md-4">
            <div class="bdrs-3 ov-h bgc-white bd">
            <div class="bgc-deep-purple-500 ta-c p-30">
                <h1 class="fw-300 mB-5 lh-1 c-white">{{ Carbon\Carbon::now()->format('M d') }}<span class="fsz-def">st</span></h1>
                <h3 class="c-white">Danas</h3>
            </div>
            <div class="pos-r">
            <!--
                <button type="button" class="mT-nv-50 pos-a r-10 t-2 btn cur-p bdrs-50p p-0 w-3r h-3r btn-warning">
                <i class="ti-plus"></i>
                </button>
            -->
                <ul class="m-0 p-0 mT-20">
                @foreach(App\Appointment::whereDate('time', \Carbon\Carbon::today())->get() as $appointment)
                    <li class="bdB peers ai-c jc-sb fxw-nw">
                        <a class="td-n p-20 peers fxw-nw mR-20 peer-greed c-grey-900" href="javascript:void(0);" data-toggle="modal" data-target="#calendar-edit">
                        <div class="peer mR-15">
                            <i class="fa fa-fw fa-clock-o c-red-500"></i>
                        </div>
                        <div class="peer">
                            <span class="fw-600">{{ $appointment->first_name }} {{ $appointment->last_name }} 
                            @switch( $appointment->barber->id % 5 )
                                @case(0)
                                    <span class="badge bgc-green-50 c-green-700 p-10 lh-0 tt-c badge-pill">{{ $appointment->barber->full_name }}</span>
                                    @break
                                @case(1)
                                    <span class="badge bgc-orange-50 c-orange-700 p-10 lh-0 tt-c badge-pill">{{ $appointment->barber->full_name }}</span>
                                    @break
                                @case(2)
                                    <span class="badge bgc-deep-purple-50 c-deep-purple-700 p-10 lh-0 tt-c badge-pill">{{ $appointment->barber->full_name }}</span>
                                    @break
                                @case(3)
                                    <span class="badge bgc-pink-50 c-pink-700 p-10 lh-0 tt-c badge-pill">{{ $appointment->barber->full_name }}</span>
                                    @break
                                @case(4)
                                    <span class="badge bgc-cyan-50 c-cyan-700 p-10 lh-0 tt-c badge-pill">{{ $appointment->barber->full_name }}</span>
                                    @break
                            @endswitch</span>
                            <div class="c-grey-600">
                            <span class="c-grey-700">{{ $appointment->time }}</span>
                            <i>{{ $appointment->service->name }}</i>
                            </div>
                        </div>
                        </a>
                    </li>
                @endforeach
                <!--
                <li class="bdB peers ai-c jc-sb fxw-nw">
                    <a class="td-n p-20 peers fxw-nw mR-20 peer-greed c-grey-900" href="javascript:void(0);" data-toggle="modal" data-target="#calendar-edit">
                    <div class="peer mR-15">
                        <i class="fa fa-fw fa-clock-o c-red-500"></i>
                    </div>
                    <div class="peer">
                        <span class="fw-600">All Day Event</span>
                        <div class="c-grey-600">
                        <span class="c-grey-700">Nov 01 - </span>
                        <i>Website Development</i>
                        </div>
                    </div>
                    </a>
                </li>
                -->
                </ul>
            </div>
            </div>
        </div>
        <div class="col-md-8">
            <div id='full-calendar'></div>
        </div>
        </div>
        <div class="modal fade" id="calendar-edit">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="bd p-15">
                <h5 class="m-0">Add Event</h5>
            </div>
            <div class="modal-body">
                <form>
                <div class="form-group">
                    <label class="fw-500">Event title</label>
                    <input class="form-control bdc-grey-200">
                </div>
                <div class="row">
                    <div class="col-md-6">
                    <label class="fw-500">Start</label>
                    <div class="timepicker-input input-icon form-group">
                        <div class="input-group">
                        <div class="input-group-addon bgc-white bd bdwR-0">
                            <i class="ti-calendar"></i>
                        </div>
                        <input type="text" class="form-control bdc-grey-200 start-date" placeholder="Datepicker" data-provide="datepicker">
                        </div>
                    </div>
                    </div>
                    <div class="col-md-6">
                    <label class="fw-500">End</label>
                    <div class="timepicker-input input-icon form-group">
                        <div class="input-group">
                        <div class="input-group-addon bgc-white bd bdwR-0">
                            <i class="ti-calendar"></i>
                        </div>
                        <input type="text" class="form-control bdc-grey-200 end-date" placeholder="Datepicker" data-provide="datepicker">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="fw-500">Event title</label>
                    <textarea class="form-control bdc-grey-200" rows='5'></textarea>
                </div>
                <div class="text-right">
                    <button class="btn btn-primary cur-p" data-dismiss="modal">Done</button>
                </div>
                </form>
            </div>
            </div>
        </div>
        </div>
    </div>
    </div>
</main>

@endsection

@section('js')
<script>

        function getColor(barber_id)
        {
            let barber_number = 5;
            switch(barber_id % barber_number)
            {
                case 0:
                    return "#388e3c";
                    break;
                case 1:
                    return "#f57c00";
                    break;
                case 2:
                    return "#512da8 ";
                    break;
                case 3:
                    return "#c2185b";
                    break;
                case 4:
                    return "#0097a7";
                    break;
                default:
                    return "black";
                    break;
            }
        }

        let events = [
            @foreach($appointments as $appointment)
            {
                title  : '{{ $appointment->first_name . ' ' . $appointment->last_name }}',
                start  : '{{ Carbon\Carbon::parse( $appointment->time ) }}',
                end: '{{ Carbon\Carbon::parse( $appointment->time )->addMinutes($appointment->service->duration) }}',
                desc   : 'Rezervacija',
                backgroundColor   :  getColor({{ $appointment->barber->id }}),
                eventTextColor: '#fff'
            },
            @endforeach
        ];
        // page is now ready, initialize the calendar...
        $('#full-calendar').fullCalendar({
            // put your options and callbacks here
            defaultView: 'agendaDay',
            events,
            header: {
                left   : 'month,agendaWeek,agendaDay',
                center : 'title',
                right  : 'today prev,next',
            },
            editable : true,
            slotEventOverlap: false, 
            
            eventBorderColor: '#fff',
            eventColor: "green",
        });
</script>
@endsection