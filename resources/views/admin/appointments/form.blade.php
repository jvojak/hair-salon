<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">

			{!! Form::myInput('text', 'first_name', 'Ime') !!}
			{!! Form::myInput('text', 'last_name', 'Prezime') !!}


			<div class="form-group">
				<label for="inputState">Servis</label>
				<select name="service_id" id="inputState" class="form-control">
				@foreach($services as $service)
				<option value="{{ $service->id }}" @if($service->id == $appointment->service_id) selected @endif >{{ $service->name }}</option>
				@endforeach
				</select>
			</div>

			<div class="form-group">
				<label for="inputState">Barber</label>
				<select name="barber_id" id="inputState" class="form-control">
				@foreach($barbers as $barber)
				<option value="{{ $barber->id }}" @if($barber->id == $appointment->barber_id) selected @endif >{{ $barber->full_name }}</option>
				@endforeach
				</select>
			</div>

<!--
			<div class="form-group">
				<span class="badge bgc-deep-purple-50 c-deep-purple-700 p-10 lh-0 tt-c badge-pill">Neriješeni zahtjev</span>
			</div>
-->
			{!! Form::myInput('dateTime', 'time', 'Datum') !!}
			{!! Form::myInput('text', 'contact', 'Kontakt broj') !!}
			{!! Form::myInput('email', 'email', 'Email') !!}
			{!! Form::myInput('text', 'service[price]', 'Cijena servisa') !!}

			<div class="form-group">
				<label for="inputState">Status</label>
				<select name="status" id="inputState" class="form-control">
					<option value="PENDING" @if($appointment->status == "PENDING") selected @endif)>Neriješeni Zahtjev</option>
					<option value="APPROVED" @if($appointment->status == "APPROVED") selected @endif)>Odobreno</option>
					<option value="DECLINED" @if($appointment->status == "DECLINED") selected @endif)>Odbijeno</option>
					<option value="COMPLETED" @if($appointment->status == "COMPLETED") selected @endif)>Naplaćeno</option>
					<option value="DID_NOT_SHOW_UP" @if($appointment->status == "DID_NOT_SHOW_UP") selected @endif)>Klijent Nije Došao</option>
				</select>
			</div>

		</div>  
	</div>
</div>