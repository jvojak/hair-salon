@extends('admin.default')

@section('page-header')
    Zakazani termini <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')
<div class="mB-20">
</div>


    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Barber</th>
                    <th>Servis</th>
                    <th>Cijena</th>
                    <th>Ime</th>
                    <th>Prezime</th>
                    <th>Email</th>
                    <th>Kontakt Broj</th>
                    <th>Vrijeme</th>
                    <th>Status</th>
                    <th>Akcije</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>ID</th>
                    <th>Barber</th>
                    <th>Servis</th>
                    <th>Cijena</th>
                    <th>Ime</th>
                    <th>Prezime</th>
                    <th>Email</th>
                    <th>Kontakt Broj</th>
                    <th>Vrijeme</th>
                    <th>Status</th>
                    <th>Akcije</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($appointments as $appointment)
                    <tr>
                        <td>{{ $appointment->id }}</td>
                        <td>{{ $appointment->barber->first_name }} {{ $appointment->barber->last_name }}</td>
                        <td>{{ $appointment->service->name }}</td>
                        <td>{{ $appointment->service->price }}</td>
                        <td>{{ $appointment->first_name }}</td>
                        <td>{{ $appointment->last_name }}</td>
                        <td>{{ $appointment->email }}</td>
                        <td>{{ $appointment->contact }}</td>
                        <td>{{ $appointment->time }}</td>
                        <td>@switch( $appointment->status )
                            @case('COMPLETED')
                                <span class="badge bgc-green-50 c-green-700 p-10 lh-0 tt-c badge-pill">Naplaćeno</span>
                                @break
                            @case('APPROVED')
                                <span class="badge bgc-orange-50 c-orange-700 p-10 lh-0 tt-c badge-pill">Odobreno</span>
                                @break
                            @case('PENDING')
                                <span class="badge bgc-deep-purple-50 c-deep-purple-700 p-10 lh-0 tt-c badge-pill">Neriješeni zahtjev</span>
                                @break
                            @case('DECLINED')
                                <span class="badge bgc-pink-50 c-pink-700 p-10 lh-0 tt-c badge-pill">Odbijeno</span>
                                @break
                            @case('DID_NOT_SHOW_UP')
                                <span class="badge bgc-red-50 c-red-700 p-10 lh-0 tt-c badge-pill">Klijent nije došao</span>
                                @break
                        @endswitch
                        </td>
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.appointments.edit', $appointment->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.appointments.destroy', $appointment->id), 
                                        'method' => 'DELETE',
                                        ]) 
                                    !!}

                                        <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>
                                        
                                    {!! Form::close() !!}
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>
@endsection