<div class="row mB-40">
	<div class="col-sm-8">
		<div class="bgc-white p-20 bd">
			{!! Form::myInput('text', 'name', 'Ime servisa') !!}
			{!! Form::myInput('text', 'description', 'Opis servisa') !!}
			{!! Form::myInput('text', 'price', 'Cijena servisa') !!}
			{!! Form::myInput('number', 'duration', 'Procijenjeno trajanje servisa') !!}

			<div class="form-group row">
			<div class="col-sm-2">Je li servis dostupan?</div>
			<div class="col-sm-10">
				<div class="form-check">
				<label class="form-check-label">
					{{ Form::hidden('available',0) }}
					{!! Form::checkbox('available') !!} 
				</label>
				</div>
			</div>
			</div>
		</div>  
	</div>
</div>