@extends('admin.default')

@section('page-header')
	User <small>{{ trans('app.update_item') }}</small>
@stop

@section('content')
	{!! Form::model($service, [
			'action' => ['ServiceController@update', $service->id],
			'method' => 'put', 
			'files' => true
		])
	!!}

		@include('admin.services.form')

		<button type="submit" class="btn btn-primary">{{ trans('app.edit_button') }}</button>
		
	{!! Form::close() !!}
	
@stop
