@extends('admin.default')

@section('page-header')
    Users <small>{{ trans('app.manage') }}</small>
@endsection

@section('content')

    <div class="mB-20">
        <a href="{{ route(ADMIN . '.services.create') }}" class="btn btn-info">
            {{ trans('app.add_button') }}
        </a>
    </div>


    <div class="bgc-white bd bdrs-3 p-20 mB-20">
        <table id="dataTable" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Ime servisa</th>
                    <th>Cijena</th>
                    <th>Opis</th>
                    <th>Dostupno</th>
                    <th>Trajanje (min)</th>
                </tr>
            </thead>
            
            <tfoot>
                <tr>
                    <th>Ime servisa</th>
                    <th>Cijena</th>
                    <th>Opis</th>
                    <th>Dostupno</th>
                    <th>Trajanje (min)</th>
                </tr>
            </tfoot>
            
            <tbody>
                @foreach ($items as $item)
                    <tr>
                        <td><a href="{{ route(ADMIN . '.services.edit', $item->id) }}">{{ $item->name }}</a></td>
                        <td>{{ $item->price }}</td>
                        <td>{{ $item->description }}</td>
                        <td>
                        @if($item->available)
                            <span class="peer">
                                <span class="badge badge-pill badge-success lh-0 p-10">Servis Dostupan</span>
                            </span>
                        @else
                            <span class="peer">
                                <span class="badge badge-pill badge-danger lh-0 p-10">Servis Nedostupan</span>
                            </span>
                        @endif
                        </td>
                        <td>{{ $item->duration }}</td>
                        <td>
                            <ul class="list-inline">
                                <li class="list-inline-item">
                                    <a href="{{ route(ADMIN . '.services.edit', $item->id) }}" title="{{ trans('app.edit_title') }}" class="btn btn-primary btn-sm"><span class="ti-pencil"></span></a></li>
                                <li class="list-inline-item">
                                    {!! Form::open([
                                        'class'=>'delete',
                                        'url'  => route(ADMIN . '.services.destroy', $item->id), 
                                        'method' => 'DELETE',
                                        ]) 
                                    !!}

                                        <button class="btn btn-danger btn-sm" title="{{ trans('app.delete_title') }}"><i class="ti-trash"></i></button>
                                        
                                    {!! Form::close() !!}
                                </li>
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        
        </table>
    </div>

@endsection