<script>
@if(Session::has('status'))
    var type = "success";
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('status') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('status') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('status') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('status') }}");
            break;
    }
@endif
</script>