<script src="https://js.pusher.com/4.4/pusher.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>

// Enable pusher logging - don't include this in production
Pusher.logToConsole = true;

var pusher = new Pusher('99d53309636b61bcb1f8', {
    cluster: 'eu',
    forceTLS: true
});

var channel = pusher.subscribe('appointments');
channel.bind('App\\Events\\NewServiceOrdered', function(data) {
      //alert(JSON.stringify(data));
      swal("Nova narudžba!", "Dobili ste novu narudžbu za šišanje");

      swal({
        title: "Nova narudžba!",
        text: "Dobili ste novu narudžbu za šišanje",
        icon: "success",
        button: "Vidi listu narudžbi!",
        }).then(function() {
            window.location = "{{ route('admin.appointments.all') }}";
        });

    });
</script>