@php
    $r = \Route::current()->getAction();
    $route = (isset($r['as'])) ? $r['as'] : '';
@endphp

<li class="nav-item mT-30">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.dash') ? 'active' : '' }}" href="{{ route(ADMIN . '.dash') }}">
        <span class="icon-holder">
            <i class="c-blue-500 ti-home"></i>
        </span>
        <span class="title">Naslovna</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.users') ? 'active' : '' }}" href="{{ route(ADMIN . '.users.index') }}">
        <span class="icon-holder">
            <i class="c-brown-500 ti-user"></i>
        </span>
        <span class="title">Administratori</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.barbers') ? 'active' : '' }}" href="{{ route(ADMIN . '.barbers.index') }}">
        <span class="icon-holder">
            <i class="c-deep-orange-500 ti-eye"></i>
        </span>
        <span class="title">Barberi</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.services') ? 'active' : '' }}" href="{{ route(ADMIN . '.services.index') }}">
        <span class="icon-holder">
            <i class="c-green-500 ti-settings"></i>
        </span>
        <span class="title">Servisi</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.appointments') ? 'active' : '' }}" href="{{ route(ADMIN . '.appointments.index') }}">
        <span class="icon-holder">
            <i class="c-deep-purple-500 ti-calendar"></i>
        </span>
        <span class="title">Kalendar</span>
    </a>
</li>
<li class="nav-item">
    <a class="sidebar-link {{ starts_with($route, ADMIN . '.appointments') ? 'active' : '' }}" href="{{ route(ADMIN . '.appointments.all') }}">
        <span class="icon-holder">
            <i class="c-red-500 ti-view-list-alt"></i>
        </span>
        <span class="title">Zauzeti termini</span>
    </a>
</li>