<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>{{ $bodyMessage }}</h2>
        <h3> <b>Barber:</b> {{ $barber }}</h3>
        <h3> <b>Usluga:</b> {{ $service }}</h3>
        <h3> <b>Cijena za platiti:</b> {{ $price }}</h3>
        <h3> <b>Vrijeme:</b> {{ $time }}</h3>


        Hvala i vidimo se!
        -- HairSalon2019 Support
    </body>
</html>