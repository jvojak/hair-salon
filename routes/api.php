<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('barbers', 'BarberController')->except([
    'create', 'index', 'edit']);
Route::resource('appointments', 'AppointmentController')->except([
    'create', 'index', 'edit']);
Route::resource('services', 'ServiceController')->except([
    'create', 'index', 'edit']);
Route::resource('worktimes', 'WorktimeController')->except([
    'create', 'index', 'edit']);;