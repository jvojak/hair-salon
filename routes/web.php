<?php

Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');
Auth::routes();

/*
|------------------------------------------------------------------------------------
| Admin
|------------------------------------------------------------------------------------
*/
Route::group(['prefix' => ADMIN, 'as' => ADMIN . '.', 'middleware'=>['auth']], function () {
    Route::get('/', 'DashboardController@index')->name('dash');
    Route::resource('users', 'UserController');

    Route::resource('barbers', 'BarberController');
    Route::get('appointments/all', 'AppointmentController@showAll')->name('appointments.all');
    Route::resource('appointments', 'AppointmentController');
    Route::resource('services', 'ServiceController');
    Route::resource('worktimes', 'WorktimeController');
});

Route::get('/', function () {
    return view('welcome');
});
